import React, {useState} from 'react';
import { 
    makeStyles, Theme, Typography, IconButton, Button, TextField, Dialog, DialogActions, DialogContent, DialogTitle, InputAdornment
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(({spacing, palette} : Theme) => ({
    input:{
        width: "30vw",
        marginBottom: "2em"
    }, 
    dialog: {
        '& .MuiDialog-paper':{
            paddingLeft: spacing(8),
            paddingRight: spacing(8),
            paddingTop: spacing(4),
            paddingBottom: spacing(4)
        }
    },
    closeButton: {
        position: 'absolute',
        right: spacing(1),
        top: spacing(1),
        color: palette.grey[500],
    },
    margin: {
        margin: spacing(1),
    },
}))

interface props {
    open: boolean;
    close: () => void;
    update: (name: string, email: string, phone:string) => void
}

function validateEmail(email: string) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const AddCustomer : React.FC<props> = ({open, close, update}) => {

    const css = useStyles()

    const [nameInput, setNameInput] = useState({value: '', error: false})
    const [emailInput, setEmailInput] = useState({value: '', error: false})
    const [phoneInput, setPhoneInput] = useState({value: '', error: false})

    const validateForm = () => {
        if( nameInput.value.length > 100){
            setNameInput({value: nameInput.value, error: true})
            return
        }
        if(!validateEmail(emailInput.value)){
            setEmailInput({value: emailInput.value, error: true})
            return
        }
        if(phoneInput.value.length > 10){
            setPhoneInput({value: phoneInput.value, error: true})
            return
        }
        update(nameInput.value,emailInput.value,phoneInput.value)
        setNameInput({value: '', error: false})
        setEmailInput({value: '', error: false})
        setPhoneInput({value: '', error: false})
        close()
    }    

    return (
        <Dialog open={open} onClose={close} className={css.dialog}>
            <IconButton className={css.closeButton} onClick={close}>
                <CloseIcon />
            </IconButton>
            <DialogTitle id="form-dialog-title">New Customer</DialogTitle>
            <DialogContent>
                <form>
                    <TextField className={css.input} placeholder="Name" error={nameInput.error} helperText={nameInput.error ? "Name exceeds 100 characters" : ""} id="standard-error" value={nameInput.value} onChange={(e) => setNameInput({value: e.target.value, error: false})} />
                    <br />
                    <TextField className={css.input} placeholder="Email" error={emailInput.error} helperText={emailInput.error ? "Invalid email format" : ""} id="standard-error" value={emailInput.value} onChange={(e) => setEmailInput({value: e.target.value, error: false})} />
                    <br />
                    <TextField
                        className={css.input}
                        id="input-with-icon-textfield"
                        InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <Typography component="span">🇮🇳 +91</Typography>
                            </InputAdornment>
                        ),
                        }}
                        error={phoneInput.error} 
                        helperText={phoneInput.error ? "Invalid phone format" : ""}
                        onChange={(e) => setPhoneInput({value: e.target.value, error: false})}
                    />
                </form>
            </DialogContent>
            <DialogActions>
                <Button onClick={close} color="primary" variant="text">
                    Cancel
                </Button>
                <Button onClick={validateForm} color="primary" variant="contained">
                    Save
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddCustomer