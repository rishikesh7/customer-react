export const data = [
    {
        "name": "Kaufman Burke",
        "email": "kaufmanburke@hivedom.com",
        "details": {
          "company":"abc company",
          "address": "370 Hyman Court, Knowlton, California, 5312",
          "telephone": "+1 (928) 572-3555",
          "language": "en",
          "timezone": "Asia/Kolkata"
      }
      },
      {
        "name": "Gutierrez Simon",
        "email": "gutierrezsimon@hivedom.com",
        "details": {
          "company":"ghj company",
          "address": "470 Hyman Court, Knowlton, California, 5312",
          "telephone": "+1 (610) 444-351945",
          "language": "en",
          "timezone": "Asia/Kolkata"
      }
      },
      {
        "name": "Johanna Spears",
        "email": "johannaspears@hivedom.com"
      },
      {
        "name": "Cooley Whitaker",
        "email": "cooleywhitaker@hivedom.com"
      },
      {
        "name": "Savannah Hicks",
        "email": "savannahhicks@hivedom.com"
      },
      {
        "name": "Helga Elliott",
        "email": "helgaelliott@hivedom.com"
      },
      {
        "name": "Melva Cummings",
        "email": "melvacummings@hivedom.com"
      },
      {
        "name": "Bettie Contreras",
        "email": "bettiecontreras@hivedom.com"
      },
      {
        "name": "Carolyn Mccray",
        "email": "carolynmccray@hivedom.com"
      },
      {
        "name": "Ellison George",
        "email": "ellisongeorge@hivedom.com"
      },
      {
        "name": "Gena Head",
        "email": "genahead@hivedom.com"
      },
      {
        "name": "Christina Wall",
        "email": "christinawall@hivedom.com"
      },
      {
        "name": "Robertson Montoya",
        "email": "robertsonmontoya@hivedom.com"
      },
      {
        "name": "Santana Stanton",
        "email": "santanastanton@hivedom.com"
      },
      {
        "name": "Schneider Haley",
        "email": "schneiderhaley@hivedom.com"
      },
      {
        "name": "Bentley Alexander",
        "email": "bentleyalexander@hivedom.com"
      },
      {
        "name": "Kerry Lopez",
        "email": "kerrylopez@hivedom.com"
      },
      {
        "name": "Aileen Douglas",
        "email": "aileendouglas@hivedom.com"
      },
      {
        "name": "Ophelia Torres",
        "email": "opheliatorres@hivedom.com",
      },
      {
        "name": "Mccormick Vega",
        "email": "mccormickvega@hivedom.com"
      },
      {
        "name": "Holland Osborn",
        "email": "hollandosborn@hivedom.com"
      },
      {
        "name": "George Mcintosh",
        "email": "georgemcintosh@hivedom.com"
      },
      {
        "name": "Erin Hopper",
        "email": "erinhopper@hivedom.com"
      },
      {
        "name": "Luna Maxwell",
        "email": "lunamaxwell@hivedom.com"
      },
      {
        "name": "Henry Callahan",
        "email": "henrycallahan@hivedom.com"
      },
      {
        "name": "Brittney Mccarty",
        "email": "brittneymccarty@hivedom.com"
      },
      {
        "name": "Estela Brown",
        "email": "estelabrown@hivedom.com"
      },
      {
        "name": "Josefina Evans",
        "email": "josefinaevans@hivedom.com"
      },
      {
        "name": "Alma Mooney",
        "email": "almamooney@hivedom.com"
      },
      {
        "name": "Essie Russell",
        "email": "essierussell@hivedom.com"
      }
    ]