import React, {useState} from 'react'
import clsx from 'clsx'
import { 
    makeStyles, Theme, Box, Typography, ListItem, Avatar, 
    ListItemAvatar, ListItemText, Drawer, IconButton, Paper,
    Table, TableBody, TableCell, TableRow, Select
} from '@material-ui/core'

import EditIcon from '@material-ui/icons/Edit';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import CloseIcon from '@material-ui/icons/Close';
import {grey} from '@material-ui/core/colors'

import { Customer } from './types'

const useStyles = makeStyles(({spacing, palette}: Theme) => ({
    drawer: {
        width: "75vw",
        padding: spacing(4)
    },
    drawerRoot:{
        '& .MuiBackdrop-root': {
            backgroundColor: "transparent"
        }
    },
    actions: {
        position: "absolute",
        top: 0,
        right: 0,
        padding: spacing(4)
    },
    header:{
        marginTop: "10vh",
        marginBottom: "8vh"
    },
    fadedText: {
        color: grey[500]
    },
    cell:{
        borderBottom: 0
    }, 
    sectionHeading:{
        marginBottom: "2vh",
        marginTop: "2vh"
    },
    tableRoot:{
        width: 0,
        padding: spacing(4)
    }
}))

interface CustomerDetailsProps {
    open : boolean;
    onClose: () => void;
    data: Customer | null;
    openUpdate: () => void
    openDelete: () => void
}

const CustomerDetails : React.FC<CustomerDetailsProps> = ({open, onClose, data, openUpdate, openDelete}) => {

    const css = useStyles();

    if ( data === null)
        return null

    const { name, email, details } = data

    return (
        <Drawer 
            open={open} 
            anchor="right" 
            onClose={onClose} 
            classes={{
                paper: css.drawer,
                root: css.drawerRoot
            }} 
            transitionDuration={{enter: 1000, exit: 1000}}
        >
            <Box className={css.actions}>
                    <IconButton onClick={openUpdate}>
                        <EditIcon color="action" />
                    </IconButton>
                    <IconButton onClick={openDelete}>
                        <DeleteOutlineIcon color="action" />
                    </IconButton>
                    <IconButton onClick={onClose}>
                        <CloseIcon color="action" />     
                    </IconButton>              
            </Box>
            <Box>
            <ListItem alignItems="flex-start" className={css.header}>
                <ListItemAvatar>
                <Avatar>{name[0].toUpperCase()}</Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={name}
                    secondary={email}
                />
            </ListItem>
            <Typography  variant="h5" className={clsx(css.fadedText,css.sectionHeading)}>DETAILS</Typography>
            <Paper elevation={3}>
                <Table classes={{root: css.tableRoot}}>
                    <TableBody>
                        <TableRow>
                            <TableCell className={css.cell}>
                                <Typography className={css.fadedText} variant="h5">Company</Typography>
                            </TableCell>
                            <TableCell className={css.cell}>
                                <Typography>{details?.company}</Typography>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className={css.cell}>
                                <Typography className={css.fadedText} variant="h5">Telephone</Typography>
                            </TableCell>
                            <TableCell className={css.cell}>
                                <Typography>{details?.telephone}</Typography>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className={css.cell}>
                                <Typography className={css.fadedText} variant="h5">Address</Typography>
                            </TableCell>
                            <TableCell className={css.cell}>
                                <Typography>{details?.address}</Typography>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell className={css.cell}>
                                <Typography className={css.fadedText} variant="h5">Language</Typography>
                            </TableCell>
                            <TableCell className={css.cell}>
                                <Select native>
                                    <option value={""}>{"Select Language"}</option>
                                </Select>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>
            <Typography  variant="h5" className={clsx(css.fadedText,css.sectionHeading)}>SPECIAL DATES</Typography>
            <Paper elevation={3}>
                <Table classes={{root: css.tableRoot}}>
                    <TableBody>
                        <TableRow>
                            <TableCell className={css.cell}>
                                <Typography className={css.fadedText} variant="h5">Birth Date</Typography>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Paper>
            </Box>
        </Drawer>
    )
}

export default CustomerDetails