import React, {useState} from 'react';
import { 
    makeStyles, Theme, Box, TextField, Typography, List, ListItem, Divider, Avatar, 
    ListItemAvatar, ListItemText, Button, Snackbar,
    Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, InputAdornment
} from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search';
import AddIcon from '@material-ui/icons/Add';
import {grey, blue} from '@material-ui/core/colors'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';

import AddCustomer from './AddCustomer';
import UpdateCustomer from './UpdateCustomer'
import CustomerDetails from './CustomerDetails'
import { Customer } from './types'
import clsx from 'clsx'

interface CustomerProps {
    data : Customer[]
}

const useStyles = makeStyles( ({ spacing } : Theme) => ({
    main:{
        padding: spacing(2)
    },
    header: {
        display: "flex",
        justifyContent: "space-between",
    },
    headerActions:{
        display: "flex",
        flexDirection: "row",
        alignContent: "center"
    },
    searchField: {
        marginRight: "5vw",
        // marginTop: spacing(2),
    },
    searchFieldBorder:{
        '& .MuiOutlinedInput-root':{
            borderRadius: "50px"
        }
    },
    addButton: {
        display: "flex",
        borderRadius: "50px",
        backgroundColor: grey[300],
        padding: spacing(2),
        '&:hover':{
            backgroundColor: grey[400]
        }
    },
    selected: {
        backgroundColor: blue[50]
    },
}))

function Alert(props: AlertProps) {
    return <MuiAlert elevation={4} variant="filled" {...props} />;
}

const CustomerComponent : React.FC<CustomerProps> = ({data}) => {

    const css = useStyles();

    const [open, setOpen] = React.useState(false);
    const [snackbar, setSnackbar] = React.useState(false);
    const [update, setUpdate] = React.useState(false)
    const [_delete, setDelete] = React.useState(false)
    const [customers, setCustomers] = React.useState(data)
    // if false does not render the detail component, if it's true then it renders the detail for the user of it's value
    const [showDetails, setShowDetails] = useState<boolean|number>(false)
 
    const handleClickOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    const handleCloseSnackbar = () => {
        setSnackbar(false);
    }

    const handleOpenSnackbar = () => {
        setSnackbar(true);
    }

    const handleUpdateClose = () => {
        setUpdate(false)
    }

    const handleDetailsClose = () => {
        setShowDetails(false)
    }

    const handleDeleteClose = () => {
        setDelete(false)
    }

    const openUpdate = () => {
        setUpdate(true)
    }

    const openDelete = () => {
        setDelete(true)
    }

    const deleteCustomer = (ix : number) => {
        setCustomers(customers.slice(0,ix).concat(customers.slice(ix+1,customers.length)))
        handleOpenSnackbar()
        setDelete(false)
    }

    const updateCustomers = (name:string, email:string, phone:string) => {
        setCustomers([{name,email,details:{telephone: phone}}, ...customers])
    }

    const updateUpdateData = (name:string, email:string, phone:string, ix:number) => {
        const updatedCustomers = [...customers]
        updatedCustomers[ix].name = name
        updatedCustomers[ix].email = email
        if("details" in updatedCustomers[ix])
            updatedCustomers[ix].details!.telephone = phone
        setCustomers(updatedCustomers)
    }

    return (
        <div className={css.main}>
            <Box className={css.header}>
                <Typography variant="h4">Customers</Typography>
                <Box className={css.headerActions}>
                    <Box>
                        <TextField
                            variant="outlined"
                            className={css.searchField}
                            classes={{
                                root: css.searchFieldBorder
                            }}
                            placeholder="Type to Search..."
                            id="input-with-icon-textfield"
                            InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon /> 
                                </InputAdornment>
                            ),
                            }}
                        />
                    </Box>
                    <Box className={css.addButton} onClick={handleClickOpen}><AddIcon/></Box>
                </Box>
            </Box>
            <Box>
                <List>
                    {
                        customers.map((customer : Customer, ix) => {
                            return (ix !== customers.length - 1 ) ? (
                                <>
                                    <ListItem className={clsx(showDetails !== false && showDetails === ix ? css.selected : null)}onClick={() => setShowDetails(ix)}>
                                        <ListItemAvatar>
                                            <Avatar variant="circle">{customer.name[0].toUpperCase()}</Avatar>
                                        </ListItemAvatar>
                                        <ListItemText primary={customer.name} secondary={customer.email} />
                                    </ListItem>
                                    <Divider />
                                </>
                            ) : (
                                <ListItem className={clsx(showDetails !== false && showDetails === ix ? css.selected : null)}onClick={() => setShowDetails(ix)}>
                                    <ListItemAvatar>
                                        <Avatar variant="circle">{customer.name[0].toUpperCase()}</Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={customer.name} secondary={customer.email} />
                                </ListItem>
                            )
                        })
                    }
                </List>
            </Box>
            <AddCustomer open={open} close={handleClose} update={updateCustomers} />
            {
                typeof(showDetails) === "number" ? (
                    <UpdateCustomer 
                        open={update} 
                        close={handleUpdateClose} 
                        update={updateUpdateData} 
                        data={{
                            name: customers[showDetails].name,
                            email: customers[showDetails].email,
                            phone: customers[showDetails].details?.telephone!,
                            ix:showDetails
                        }}
                    />
                ) : null
            }
            {
                typeof(showDetails) === "number" ? (
                    <Dialog open={_delete} onClose={handleDeleteClose} >
                        <DialogTitle id="alert-dialog-title">{"Delete Customer"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are you sure you want to continue?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleDeleteClose}>Cancel</Button>
                            <Button onClick={() => deleteCustomer(showDetails)} color="primary" variant="contained">Confirm</Button>
                        </DialogActions>
                    </Dialog>
                ) : null
            }
            <CustomerDetails 
                open={showDetails !== false} 
                onClose={handleDetailsClose} 
                data={typeof(showDetails) === "number" ? customers[showDetails] : null} 
                openUpdate={openUpdate}
                openDelete={openDelete}
            />
            <Snackbar open={snackbar} onClose={handleCloseSnackbar} autoHideDuration={6000}>
                <Alert onClose={handleCloseSnackbar} severity="info">
                    Customer Deleted Successfully!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default CustomerComponent;