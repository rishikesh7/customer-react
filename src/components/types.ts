export type CustomerDetails = {
    company ?: string;
    address ?: string;
    telephone ?: string;
    language ?: string;
    timezone ?: string;
}

export type Customer = {
    name: string;
    email: string;
    details?: CustomerDetails
}
