import React from 'react';
import CustomerComponent from "../components/customer"
import { data } from '../components/userdata.js'

export default {
    title: "Customer",
    component: CustomerComponent
}

export const customer = () => (<CustomerComponent data={data} />)